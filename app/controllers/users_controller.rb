##
# Controller for User objects. Handles routing for the following:
# - Registration
# - Directory Listing
# - Directory Search [possibly]
# - Viewing User Profiles
# - Editing User Information
# - Deleting Users
class UsersController < ApplicationController
  def new
  end

  ##
  # Saves a new User submitted through the signup form in #new
  def create
    data = params.require(:user).permit(:name, :username, :email)
    @user = User.new(data)
    @user.save
    redirect_to @user
  end

  def index
    @users = User.all
  end

  def edit
  end

  def update
  end

  ##
  # Displays a single user
  def show
    @user = User.find(params[:id])
  end

  def destroy
  end
end
