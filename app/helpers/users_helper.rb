##
# Provides utilities for displaying Users
module UsersHelper

  ##
  # Returns a gravatar url for a user
  def gravatar_url(user,size=50,d="identicon")
    id = Digest::MD5::hexdigest user.email.downcase
    return "https://secure.gravatar.com/avatar/#{id}?s=#{size}&d=#{d}"
  end

end
