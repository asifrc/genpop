# Bismillah

##
# Deploys repo to heroku after adding ssh keys through bash
desc "Deploy project to Heroku"
task :deploy do

  # Pipes the following through bash
  # - Start the ssh-agent
  # - Add ssh keys
  # - Git push to Heroku
  output = %x(echo eval `ssh-agent -s` ^&^& echo ssh-add /C/Users/Asif/.ssh/heroku.genpop ^&^& echo git push heroku master | bash)
  
  # Kill the ssh-agent process started by bash
  pidtext = /Agent pid [0-9]+/.match output
  pid = /[0-9]+/.match pidtext.to_s
  sh "kill #{pid.to_s}"
  
  puts "Deploy Task Complete"
  
end