require 'spec_helper'

describe "UserPages" do
  describe "SignUp" do
    it "responds to /signup" do
      get signup_path
      response.status.should be(200)
    end
    it "adds a user on submission" do
      visit '/signup'
      fill_in "Name", with: "Test User"
      fill_in "Username", with: "mctest22"
      fill_in "Email", with: "test@example.com"
      expect { click_button "Create Account" }.to change(User, :count).by(1)
    end
  end
  describe "users" do
    it "responds to /users" do
      get users_path
      response.status.should be(200)
    end
  end
end
