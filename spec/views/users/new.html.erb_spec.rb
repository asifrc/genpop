require 'spec_helper'

describe "users/new.html.erb" do
  it "should contain the phrase 'Sign Up'" do
    visit '/signup'
    expect(page).to have_content "Sign Up"
  end
end
