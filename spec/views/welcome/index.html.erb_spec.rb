require 'spec_helper'

describe "welcome/index.html.erb" do
  # Bismillah
  it "should contain the word 'Bismillah'" do
    visit '/'
    expect(page).to have_content 'Bismillah'
  end
end
